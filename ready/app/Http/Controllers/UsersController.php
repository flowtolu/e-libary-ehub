<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\Users\CreateRequest;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'head.librarian']); // head.librarian
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activitylog = DB::insert('insert into activitylog_beta (user_id, action) values (?, ?)', [Auth::user()->id, Auth::user()->name.' Visted Login User Management Page']);
        $users = User::latest()->get();

        return view('librarian.users.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('librarian.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        User::create($request->all());

       
        $activitylog = DB::insert('insert into activitylog_beta (user_id, action) values (?, ?)', [Auth::user()->id, 'New user ('.$request->input('username').') created']);
         flash()->success('Created', 'New login user created successfully');  
        return redirect('users'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $status = $user->status ? 0 : 1;

        $user->update(['status' => $status]);

        flash()->success('Updated', 'User status updated successfully');
        return back(); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
