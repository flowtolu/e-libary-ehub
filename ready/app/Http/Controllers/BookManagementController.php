<?php

namespace App\Http\Controllers;

use App\Book;
use DB;
use Auth;
use App\Borrow;
use App\History;
use App\Http\Requests\Carts\CartRequest;
use App\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BookManagementController extends Controller
{
    public function borrow()
    {
        $books = Book::orderBy('title')->with('category', 'author')->get();

    	return view('librarian.borrow.index', compact('books'));
    }

    public function borrowBooks(CartRequest $request)
    {
        $books = $request->books;
        $member = $request->member;
        foreach($books as $book) {
            $borrow = Borrow::create([
                'book_id' => $book['id'],
                'member_id' => $member,
                'copies' => 1, // remove
                'expected_return_date' => Carbon::now()->addDays($book['copies']), //
                'borrow_date' => Carbon::now() 
            ]);

            $borrow->book->decrement('available_copy', $book['copies']);
            $history = new History;
            $history->borrow_id = $borrow->id;
            $history->copies = 1;
            $history->save();
        }

        flash()->success('Successfull', 'Borrow Book entry stored successfully');
        
        $username = DB::table('members')->where('id', $request->member)->first();
        $activitylog = DB::insert('insert into activitylog_beta (staff, action) values (?, ?)', [Auth::user()->username, $username->name.' borrowed a book.']);
        return response()->json(['success' => 'Borrow book succedded'], 200);
    }

    public function getReturn(Request $request)
    {
    	$books = (new Borrow)->newQuery();
    	if($request->has('member') && $request->member !== 'all')
		{
			$books->where('member_id', $request->member);
		}
		$books->where(function($query){
            /**
			$query->where(
				\DB::raw('TIMESTAMPDIFF(DAY, borrow_date, return_date)'), '>', 
				config('library.settings.penalty_after')
			);
            **/
			$query->whereNull('return_date');	
		});
		$books->orderBy('borrow_date', 'desc');
		$books->with('member', 'book.category');

		$result = $books->get();

        
    	return view('librarian.return.index', [
    		'books' => $result
    	]);
    }

    public function borrowedBooks()
    {
        
    }

    public function return(Borrow $borrow)
    {
    	$borrow->update(['return_date' => Carbon::now()]);
        $borrow->book->increment('available_copy', $borrow->copies);

        $history = new History;
        $history->borrow_id = $borrow->id;
        $history->copies = $borrow->copies;
        $history->type = 'return';
        $history->save();

        $getname = DB::table('borrows')->where('id', $borrow->id)->first();
        $getusername = DB::table('members')->where('id', $getname->member_id)->first();
    	flash()->success('Returned', 'book return entry placed. keep book into self');
        $activitylog = DB::insert('insert into activitylog_beta (staff, action) values (?, ?)', [Auth::user()->name, 'Book returned by ' .$getusername->name.'.']);
        
    	return back();
    }

    public function unavailable()
    {
        $members = Member::with(['borrows' => function($query){
            $query->where('return_date', null);
            $query->with('book');
        }])->whereHas('borrows', function ($query) {
            $query->where('return_date', null);
        })
        ->get(); 

        return view('librarian.reports.unavailableBooks', [
            'members' => $members
        ]);
    }

 public function returned(Request $request)
    {
       $histories = (new History)->newQuery();
        $histories->with('borrow.book.category', 'borrow.member');
        if($user = $request->has('user')){
            if($user !== 'all'){
                $histories->whereHas('borrow', function($query) use($user) {
                    $query->where('member_id', $user);
                });
            }
        }
        if($type = $request->has('type')){
            if($type !== 'all')
                $histories->where('type', $type);
        }
        $histories->where('created_at', '>', Carbon::now()->subMonth());
        $histories->orderBy('created_at', 'desc');
        
        return view('librarian.reports.returnedbooks', [
            'histories' => $histories->get()
        ]);
    }
   

 public function borrowedBooks1(Request $request)
    {
       $histories = (new History)->newQuery();
        $histories->with('borrow.book.category', 'borrow.member');
        if($user = $request->has('user')){
            if($user !== 'all'){
                $histories->whereHas('borrow', function($query) use($user) {
                    $query->where('member_id', $user);
                });
            }
        }
        if($type = $request->has('type')){
            if($type !== 'all')
                $histories->where('type', $type);
        }
        $histories->where('created_at', '>', Carbon::now()->subMonth());
        $histories->orderBy('created_at', 'desc');
        
        return view('librarian.reports.borrowedBooks', [
            'histories' => $histories->get()
        ]);
    }

 }
