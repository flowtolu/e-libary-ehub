<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
	/**
	 * Constructor function
	 */
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Dashboard page for authenticated users
     * @return [type] [description]
     */
    public function index(Request $request)
    {
        $activitylog = DB::insert('insert into activitylog_beta (staff, action) values (?, ?)', [Auth::user()->name, Auth::user()->name.' Visted Dashboard']);
        return view('librarian.dashboard');
    }
}
