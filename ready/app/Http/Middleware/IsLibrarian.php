<?php

namespace App\Http\Middleware;

use Closure;

class IsLibrarian
{

//     public function findWord($caseSensitive, $testString, $findWord) {

//     if ($caseSensitive === true) {
//         if (strpos($testString, $findWord) !== false) {
//             return true;
//         } else {
//             return false;
//         }
//     } else {
//         if (strpos(strtolower($testString), strtolower($findWord)) !== false) {
//             return true;
//         } else {
//             return false;
//         }
//     }

// }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( ! auth()->check() ) {
            return redirect('login');
        }
        // if(strpos(strtolower(auth()->user()->type), strtolower(' librarian')) == false){ // auth()->user()->type !== 'librarian' 
        //     flash()->error('Unauthorized', 'you are not authorized for this action');
        //     return redirect('dashboard');
        // }

        return $next($request);
    }
}
