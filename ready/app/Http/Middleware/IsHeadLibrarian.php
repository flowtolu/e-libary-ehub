<?php

namespace App\Http\Middleware;

use Closure;

class IsHeadLibrarian
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( ! auth()->check() ) {
            return redirect('login');
        }
        // if(auth()->user()->type !== 'Head librarian') {
        //     flash()->error('Unauthorized', 'you are not authorized for this action');
        //     return redirect('dashboard');
        // }

        return $next($request);
    }
}
