<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        User::create([
           'id' => 1,
            'name' => 'Admin',
            'address' => 'Gjorgjijoski',
            'username' => 'admin',
            'phone' => '455664433',
            'type' => 'Head librarian',
            'password' => '123456',
        ]);        
    }
}
