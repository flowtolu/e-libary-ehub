<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StudentData', function (Blueprint $table) {
            $table->text('Surname');
            $table->text('Firstname');
            $table->text('Middlename');
            $table->text('Department');
            $table->text('Jambno');
            $table->text('faculty');
            $table->text('phoneno');
            $table->text('studenttype');
            $table->text('id');
            $table->text('email');
            $table->text('contactaddress');
            $table->text('permanentaddress');
            $table->text('bloodgroup');
            $table->text('sex');
            $table->text('modeofentry');
            $table->text('graduationyear');
            $table->text('entryyear');
            $table->text('studentmode');
            $table->text('schoolprogram');
            $table->text('religion');
            $table->text('modeofstudy');
            $table->text('yearofstudy');
            $table->text('departmentoption');
            $table->text('passportdata');
            $table->text('dateofbirth');
            $table->text('matricno');
            $table->text('genotype');
            $table->text('state');
            $table->text('LGA');
            $table->text('Country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
