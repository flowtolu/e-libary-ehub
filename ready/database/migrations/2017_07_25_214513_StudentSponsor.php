<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentSponsor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StudentSponsor', function (Blueprint $table) {
            $table->text('MatricNo');
            $table->text('StudentId');
            $table->text('FullName');
            $table->text('Address');
            $table->text('PhoneNo');
            $table->text('Email');
            $table->text('Relationship');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
