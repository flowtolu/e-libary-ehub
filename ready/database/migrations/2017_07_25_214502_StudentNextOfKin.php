<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentNextOfKin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StudentNextOfKin', function (Blueprint $table) {
            $table->text('StudentId');
            $table->text('MatricNo');
            $table->text('FullName');
            $table->text('Address');
            $table->text('PhoneNo');
            $table->text('Email');
            $table->text('Relationship');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
