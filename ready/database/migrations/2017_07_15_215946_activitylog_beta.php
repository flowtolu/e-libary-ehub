<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivitylogBeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
Schema::create('activitylog_beta', function (Blueprint $table) {
            $table->increments('log_id');
            $table->string('timestamp')->nullable();
            $table->string('staff');
            $table->integer('user_id')->nullable();
            $table->integer('member_id')->nullable();
            $table->string('action')->nullable();
           
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
