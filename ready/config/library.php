<?php 

return [
	'settings' => [
		'penalty_option' => 1,
		'penalty_rate'   => 5,
		'penalty_after'  => 7
	]
];