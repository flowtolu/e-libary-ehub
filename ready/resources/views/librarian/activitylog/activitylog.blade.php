@extends('layouts.master')

@section('pg-title', 'Activity log')
@section('pg-head-left', ' Activity log ')

@section('content')

<div class="table-responsive">
<table class="table table-bordered table-striped table-hover dataTable datatable">
        <thead class="">
   	         <tr class="active">
            	<th>Log id</th>
            	<th>Time</th>
                <th>Staff</th>
            	<th>Action</th>
            </tr>
        </thead>

        <tbody>
        @foreach($activitylog as $al)
          	 <tr>
               <td> {{$al->log_id}}</td>
               <td> {{$al->timestamp}}</td>
               <td> {{$al->staff}}</td>
               <td> {{$al->action}}</td>
            </tr> 
        @endforeach
        </tbody>
    </table>
</div>


  
@stop

