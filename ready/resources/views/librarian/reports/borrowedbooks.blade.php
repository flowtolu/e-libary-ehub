@extends('layouts.master')

@section('pg-title', 'borrowed bboks')
@section('pg-head-left', 'Members with Books that have been borrowed')

@section('content')


<table class="table table-bordered table-striped table-hover dataTable datatable">
         <table class="table table-bordered table-striped table-hover dataTable datatable">
        <thead>
            <tr class="active">
                <th>Member Code</th>
                <th>Member Name</th>
                <th>Book Name</th>
                <th>Book Category</th>
                <th>Borrowed date</th>
               
                
               
            </tr>
        </thead>



<tbody>
          @if(count($histories))
        @foreach($histories as $history)
            <tr>
                <td>
                    {{ $history->borrow->member->code ? : 'XXXX' }}
                </td>
                <td>
                    {{ $history->borrow->member->name }}
                </td>
                <td>
                    {{ $history->borrow->book->title }}
                </td>
                <td>
                    {{ $history->borrow->book->category->title }}
                </td>
                <td>
                    {{ $history->created_at->format('d M h:i A') }}
                </td>
                
                



            </tr>
        @endforeach 
        @else
            <tr>
                <td colspan="8">
                    <p class="text-center" style="font-size: large;">
                        no history found to display.
                    </p>        
                </td>
            </tr> 
        @endif    
        </tbody>







          
  </table>
@stop

