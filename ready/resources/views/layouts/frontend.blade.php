@inject('galleries', 'App\Services\GalleryService')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>eLibrary - Home Page</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Latest compiled and minified bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Material Design Bootstrap | not found -->
    <link href="{{ asset('frontend/mdb.min.css') }}" rel="stylesheet">

    <!-- Template styles | not found -->
    <link href="{{ asset('frontend/style.css') }}" rel="stylesheet">

</head>

<body>
    <!--Navbar classes: navbar-toggleable-md navbar-dark scrolling-navbar -->
    <nav class="navbar navbar-inverse fixed-top">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapseEx2" aria-controls="collapseEx2" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="{{ url('home') }}">
            <p>click here to login to dashboard...</p>
                {{-- <img src="{{ url('images/logo.png') }}" class="d-inline-block align-top z-depth-0" alt="MDBootstrap" width="150px"> --}}
            </a>
            <div class="collapse navbar-collapse" id="collapseEx2">
                <ul class="navbar-nav navbar-right">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('login') }}">Login to Admin <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!--/.Navbar-->
    <div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">E Library Demo</div>

                <div class="panel-body">
                        <p>This is a demo of the E library platform for feedback and appoval.</p>
                        <p>Please use the links in the navigation bar above to view the different sections</p>
                        <p>Please note that you must be logged in to view the demo.</p>
                        <p>Login details are available on request.</p>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('frontend/jquery.min.js') }}"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('frontend/tether.min.js') }}"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('frontend/bootstrap.min.js') }}"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('frontend/mdb.min.js') }}"></script>


</body>

</html>