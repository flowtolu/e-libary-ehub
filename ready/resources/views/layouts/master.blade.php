<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>eLibrary - @yield('pg-title')</title>
    <!-- Favicon-->
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- <link rel="stylesheet" href="/frontend/bootstrap.min.css">
    <link rel="stylesheet" href="/frontend/bootstrap.min.js"> -->
    @yield('styles')
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
    .sidebar .menu .list li.active > :first-child span,
    .sidebar .menu .list li.active > :first-child i {
        color: #F44336;
        font-weight: bold;
background: url('background_image-03.jpg') -47px 0;

    }
    </style>
</head>

<body class="theme-red" style="background-image: url('/images/background_image-03.jpg')">
    <body class="theme-red">

    <!-- Page Loader -->
    @include('layouts.master.loader')
    
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    
    <!-- Search Bar -->
    @include('layouts.master.searchbar')
    
    <!-- Top Bar -->
    @include('layouts.master.topbar')
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        @include('layouts.master.leftsidebar')
        
        <!-- Right Sidebar -->
        @include('layouts.master.rightsidebar')
    </section>

    <section class="content">
        <div class="container-fluid">
           <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>@yield('pg-head-left')</h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right">
                                    @yield('pg-head-right')
                                </div>
                            </div>
                        </div>
                        <div class="body" id="app">
                            @yield('content')
                        </div>
                    </div>
                </div>

                {{-- <div class="container-fluid" style="text-align: center;  margin-left: 65px;">

                <div class="col-md-10" style="background-color: #ffffff; border: 1px dashed red;  position: relative;">
                    <img src="/images/library_access_scan.jpg"><br><br>
                    <img src="/images/librabry_access_scan_text.jpg">
                    <hr>
                    <div class="row">
                    
                    <div class="col-md-3">
                    <img src="/images/manage-_library.jpg">
                    <img src="/images/manage_library_text.jpg">
                    </div>

                    <div class="col-md-3">
                    <img src="/images/manage_categories.jpg">
                     <img src="/images/manage_categories_text.jpg">
                    </div>

                    <div class="col-md-3">
                    <img src="/images/manage_book_info.jpg">
                    <img src="/images/manage_categories_text.jpg">
                    </div>

                    <div class="col-md-3">
                    <img src="/images/library_access_scan.jpg">
                    <img src="/images/manage_categories_text.jpg">
                    </div>
<br> <br> <br>



                     <div class="row ">
                    <br><br><br> <br> <br>
                    <div class="col-md-3">
                    <img src="/images/student_list.jpg" >
                    <br>
                    <img src="/images/student_list_text.jpg">
                    </div>

                    <div class="col-md-3">
                    <img src="/images/message_communication.jpg">
                    <br> <br>
                     <img src="/images/message_communication_text.jpg">
                    </div>

                    <div class="col-md-3">
                    <img src="/images/report.jpg">
                    <br>
                    <img src="/images/report_text.jpg">
                    </div>

                    <div class="col-md-3">
                    <img src="/images/logout.jpg">
                    <br>
                    <img src="/images/logout_text.jpg">
                    </div>


 --}}


                    </div><br><br>
                    
                </div>

                </div>


            </div>
        </div>
    </section>


    <script src=" {{ asset("js/app.js") }}"></script>
    <script src=" {{ asset("js/vendor.js") }}"></script>
    @include('layouts.partials.flash')
    @yield('scripts')
</body>

</html>