<!--From layouts.fronend-->
    <!--Carousel Wrapper-->
    <div id="carousel-example-1" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
        <?php $i = 0; ?>
          @foreach($galleries->all() as $gallery)
            <!--First slide-->
            <div class="carousel-item {{ $i == 0 ? 'active' : '' }}" style="background-image: url('{{ url('images/gallery/' . $gallery->path) }}');background-repeat: no-repeat;background-size: cover">
                <!--Mask-->
                <div class="view hm-black-light">
                    <div class="full-bg-img flex-center">
                        <ul class="animated fadeInUp col-md-12">
                            <li>
                                <h1 class="h1-responsive flex-item">Material Design for Bootstrap 4</h1>
                            </li>
                            <li>
                                <p class="flex-item">The most powerful and free UI KIT for Bootstrap</p>
                            </li>
                            <li>
                                <a target="_blank" href="https://mdbootstrap.com/getting-started/" class="btn btn-primary btn-lg flex-item">Sign up!</a>
                            </li>
                            <li>
                                <a target="_blank" href="https://mdbootstrap.com/material-design-for-bootstrap/" class="btn btn-default btn-lg flex-item">Learn more</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--/.Mask-->
            </div>
          <?php $i++; ?>  
          @endforeach
        </div>
        <!--/.Slides-->
        <!--Indicators-->
        <ol class="carousel-indicators">
          @for($j = 0; $j < $i; $j++)
            <li data-target="#carousel-example-1" data-slide-to="{{ $j }}" class="{{ $j == 0 ? 'active' : '' }}"></li>
          @endfor
        </ol>
        <!--/.Indicators-->

        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-1" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-1" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
    <!--End from layouts.fronend-->